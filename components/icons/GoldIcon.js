import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default () => (
    <FontAwesomeIcon icon={"check-circle"} color="rgb(229,193,0)" />
);
